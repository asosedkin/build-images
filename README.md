# GnuTLS -- Building CI images

This GnuTLS sub-project generates and pushes to gitlab.com
docker registry the docker images to be used for compiling the
master branch of the library.

The reason for pre-generating the images is to speed-up CI runs
and avoid failures due to downloading of individual packages (e.g.,
because some mirrors were down).

# How to generate a new image

Add a new directory with a Dockerfile containing the instructions
for the image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.


# How to re-generate an existing image

Visit the [container registry page](https://gitlab.com/gnutls/build-images/container_registry) of the
gitlab project. Then
 * push (possibly with adding a tag)

The image will be re-build.
